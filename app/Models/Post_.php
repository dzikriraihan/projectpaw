<?php

namespace App\Models;

class Post
{
    private static $listMakanan = [
        [
            "name" => "Nasi Goreng",
            "slug" => "nasi-goreng",
            "price" => "Rp 15.000,00"
        ], 
        [
            "name" => "Mie Goreng",
            "slug" => "mie-goreng",
            "price" => "Rp 13.000,00"
        ],
        [
            "name" => "Kwetiau",
            "slug" => "kwetiau",
            "price" => "Rp 14.000,00"
        ],
        [
            "name" => "Mie Jawa",
            "slug" => "mie-jawa",
            "price" => "Rp 17.000,00"
        ]
    ];

    private static $listMinuman = [
        [
            "name" => "Es Teh",
            "slug" => "es-teh",
            "price" => "Rp 3.000,00"
        ], 
        [
            "name" => "Es Jeruk",
            "slug" => "es-jeruk",
            "price" => "Rp 4.000,00"
        ],
        [
            "name" => "Milo",
            "slug" => "milo",
            "price" => "Rp 7.000,00"
        ],
        [
            "name" => "Es Coklat",
            "slug" => "es-coklat",
            "price" => "Rp 7.000,00"
        ],
        [
            "name" => "Air Putih",
            "slug" => "air-putih",
            "price" => "Rp 2.000,00"
        ]
    ];

    public static function all() {
        return self::$listMakanan;
    }

    public static function allDrinks() {
        return self::$listMinuman;
    }
}
