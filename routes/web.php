<?php

use App\Models\Post;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\OrderController;

Route::get('/', [PostController::class, 'index']);
Route::get('/drinks', [PostController::class, 'drinks']);
Route::get('/add', [PostController::class, 'add']);
Route::get('/update', [PostController::class, 'update']);
Route::get('/update/updateFood', [PostController::class, 'updateFood']);
Route::get('/update/updateDrinks', [PostController::class, 'updateDrinks']);
Route::get('/update/updateFood/{menu}', [PostController::class, 'show']);
Route::get('/update/updateDrinks/{menu}', [PostController::class, 'showDrinks']);
Route::post('/add/create', [PostController::class, 'store'])->name('food.store');
Route::get('update/updateFood/{menu}/deleteFood', [PostController::class, 'delete'])->name('food.delete');
Route::get('update/updateDrinks/{menu}/deleteDrink', [PostController::class, 'deleteDrinks'])->name('drinks.delete');
Route::put('update/updateFood/{menu}/updateFood', [PostController::class, 'updateMakanan'])->name('food.update');
Route::put('update/updateDrinks/{menu}/updateDrinks', [PostController::class, 'updateMinuman'])->name('drinks.update');
Route::post('/orders', [OrderController::class, 'store'])->name('orders.store');
Route::get('/check', [OrderController::class, 'check'])->name('check.order');
Route::post('/finish', [OrderController::class, 'finishOrder'])->name('finish.order');

